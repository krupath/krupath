Hello, folks! <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px"> \
I'm Krupa Thakkar, a researcher in pscyhology, neuroscience and healthcare

![](https://img.shields.io/badge/OS-LINUX-informational?style=flat&logo=data:image/svg%2bxml;base64,<BASE64_DATA>)

![](https://img.shields.io/badge/Editor-vim-informational?style=flat&logo=data:image/svg%2bxml;base64,<BASE64_DATA>)

![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=data:image/svg%2bxml;base64,<BASE64_DATA>)

![](https://img.shields.io/badge/Code-R-informational?style=flat&logo=data:image/svg%2bxml;base64,<BASE64_DATA>)

![](https://img.shields.io/badge/Shell-Bash-informational?style=flat&logo=data:image/svg%2bxml;base64,<BASE64_DATA>)
<!-- Actual text -->

You can find me on [![Twitter][1.2]][1], or on [![LinkedIn][2.2]][2].

<!-- Icons -->

[1.2]: http://i.imgur.com/wWzX9uB.png (twitter icon without padding)
[2.2]: https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/linkedin-3-16.png (LinkedIn icon without padding)

<!-- Links to your social media accounts -->

[1]: https://twitter.com/kru_path
[2]: https://www.linkedin.com/in/krupath/

